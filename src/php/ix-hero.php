<?php
//Administration part

//Add links to the installed pluing overview page
add_filter( 'plugin_row_meta', 'ix_hero_plugin_row_links', 10 ,2 );

function ix_hero_plugin_row_links( $links, $file ) {
	if ( $file == plugin_basename( __FILE__ ) ) {
		$new_links = array(
					'<a href="https://bitbucket.org/aidik/ix-hero/issues/" target="_blank">Report Bugs</a>',
					'<a href="'. plugin_dir_url( __FILE__ ) .'readme.pdf" target="_blank">Read Me</a>'
				);

		$links = array_merge( $links, $new_links );
	}

	return $links;
}

//Register post type
add_action( 'init', 'register_ix_hero_post_type' );
function register_ix_hero_post_type() {
	$labels = array(
		'name'				=> __( 'iX Hero Images', 'ix-hero' ),
		'singular_name'		=> __( 'iX Hero Image', 'ix-hero' ),
		'menu_name'			=> __( 'iX Hero Images', 'ix-hero' ),
		'name_admin_bar'	=> __( 'iX Hero Image', 'ix-hero' ),
		'add_new'			=> __( 'Add New', 'ix-hero' ),
		'add_new_item'		=> __( 'Add New iX Hero Image', 'ix-hero' ),
		'new_item'			=> __( 'New iX Hero Image', 'ix-hero' ),
		'edit_item'			=> __( 'Edit iX Hero Image', 'ix-hero' ),
		'view_item'			=> __( 'View iX Hero Image', 'ix-hero' ),
		'all_items'			=> __( 'All iX Hero Images', 'ix-hero' ),
		'search_items'		=> __( 'Search iX Hero Images', 'ix-hero' ),
		'not_found'			=> __( 'Not even one iX Hero Image found.', 'ix-hero' ),
		'not_found_in_trash'=> __( 'Not even one iX Hero Image found in Trash.', 'ix-hero' )
	);

	$args = array(
		'labels'			=> $labels,
		'description'		=> __( 'iX Hero Images to be displayed in your post and or page.', 'ix-hero' ),
		'public'			=> false,
		'publicly_queryable'=> false,
		'show_ui'			=> true,
		'show_in_menu'		=> true,
		'query_var'			=> true,
		'rewrite'			=> array( 'slug' => 'ix-hero' ),
		'capability_type'	=> 'post',
		'has_archive'		=> false,
		'hierarchical'		=> false,
		'menu_position'		=> 15,
		'supports'			=> array( 'title' ),
		'menu_icon'			=> plugin_dir_url( __FILE__ ) . '/img/ix-hero.png'
	);

	register_post_type( 'ix_hero', $args );
}

add_action( 'add_meta_boxes', 'ix_hero_add_html_boxes' );
function ix_hero_add_html_boxes() {

		add_meta_box( 'ix_hero_html_box', __( 'Insert custom HTML', 'ix-hero' ), 'render_html_box', 'ix_hero' );
		add_meta_box( 'ix_hero_css_box', __( 'Insert custom CSS', 'ix-hero' ), 'render_css_box', 'ix_hero' );
		add_meta_box( 'ix_hero_img_box', __( 'Select Images', 'ix-hero' ), 'render_img_box', 'ix_hero' );
		add_meta_box( 'ix_hero_shorcode_box', __( 'Shortcode to insert into posts', 'ix-hero' ), 'render_shortcode_box', 'ix_hero' );


	}


function render_html_box( $post ) {

		// Lets do something for security
		wp_nonce_field( 'ix_hero_action', 'ix_hero_nonce' );

		// Get existing value from db
		$html = get_post_meta( $post->ID, 'html', true );


		// If db is empty get the defaults from the file
		if( empty( $html ) ) $html = file_get_contents ( plugin_dir_path( __FILE__ ) . '/defaultHtml.txt' );

		// Html Code
		echo '<textarea style="width: 96%; height: 300px;" id="html" name="html">';
		echo $html ;
		echo '</textarea>';

	}

function render_css_box( $post ) {

		// Get existing value from db
		$css = get_post_meta( $post->ID, 'css', true );


		// If db is empty get the defaults from the file
		if( empty( $css ) ) $css = file_get_contents ( plugin_dir_path( __FILE__ ) . '/defaultCss.txt' );

		// Html Code
		echo '<textarea style="width: 96%; height: 300px;" id="css" name="css">';
		echo $css ;
		echo '</textarea>';

	}

function render_img_box( $post ) {

		// Get existing value from db
		$large_image = get_post_meta( $post->ID, 'large_image', true );
		$medium_image = get_post_meta( $post->ID, 'medium_image', true );
		$small_image = get_post_meta( $post->ID, 'small_image', true );
		$micro_image = get_post_meta( $post->ID, 'micro_image', true );


		// If db is empty fill variables with empty strings
		if( empty( $large_image ) ) $large_image = '';
		if( empty( $medium_image ) ) $medium_image = '';
		if( empty( $small_image ) ) $small_image = '';
		if( empty( $micro_image ) ) $micro_image = '';

		// Absolutize image urls
		$url = get_site_url();
		if ( $large_image[0] == "/") {
			$large_image = $url . $large_image;
		}

		if ( $medium_image[0] == "/") {
			$medium_image = $url . $medium_image;
		}

		if ( $small_image[0] == "/") {
			$small_image = $url . $small_image;
		}

		if ( $micro_image[0] == "/") {
			$micro_image = $url . $micro_image;
		}


		// Html Code
		echo '<table class="form-table"><tbody><tr><th scope="row">';
		echo '<label for="large_image">' . __( 'Large Image', 'ix-hero' ) . '</label></th>';
		echo '<td><input type="text" id="large_image" name="large_image" placeholder="' . esc_attr__( '', 'ix-hero' ) . '" value="' . esc_attr__( $large_image ) . '">';
		echo '<input type="button" id="large-image-button" class="button" value="' . __( 'Select Image', 'ix-hero' ) . '" /></td></tr>';

		echo '<tr><th scope="row">';
		echo '<label for="medium_image">' . __( 'Medium Image', 'ix-hero' ) . '</label></th>';
		echo '<td><input type="text" id="medium_image" name="medium_image" placeholder="' . esc_attr__( '', 'ix-hero' ) . '" value="' . esc_attr__( $medium_image ) . '">';
		echo '<input type="button" id="medium-image-button" class="button" value="' . __( 'Select Image', 'ix-hero' ) . '" /></td></tr>';

		echo '<tr><th scope="row">';
		echo '<label for="small_image">' . __( 'Small Image', 'ix-hero' ) . '</label></th>';
		echo '<td><input type="text" id="small_image" name="small_image" placeholder="' . esc_attr__( '', 'ix-hero' ) . '" value="' . esc_attr__( $small_image ) . '">';
		echo '<input type="button" id="small-image-button" class="button" value="' . __( 'Select Image', 'ix-hero' ) . '" /></td></tr>';

		echo '<tr><th scope="row">';
		echo '<label for="micro_image">' . __( 'Micro Image', 'ix-hero' ) . '</label></th>';
		echo '<td><input type="text" id="micro_image" name="micro_image" placeholder="' . esc_attr__( '', 'ix-hero' ) . '" value="' . esc_attr__( $micro_image ) . '">';
		echo '<input type="button" id="micro-image-button" class="button" value="' . __( 'Select Image', 'ix-hero' ) . '" /></td></tr></tbody></table>';
		echo '<br />';




	}

function render_shortcode_box( $post ) {

		// Html Code
		echo '[ix-hero id="' . $post->ID . '"]';
		echo '<br />';
		echo '<p>' . __( "Don't forget to save this iX-Hero Image before using the Shortcode.", 'ix-hero' ) . '</p>';



	}

add_action( 'save_post', 'ix_hero_save_metabox' );

function ix_hero_save_metabox( $post_id ) {

	// Add nonce for security and authentication.
	$nonce_name   = $_POST['ix_hero_nonce'];
	$nonce_action = 'ix_hero_action';

	// Check if a nonce is set.
	if ( ! isset( $nonce_name ) )
		return;

	// Check if a nonce is valid.
	if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
		return;

	// Check if the user has permissions to save data.
	if ( ! current_user_can( 'edit_post', $post_id ) )
		return;

	// Get user input
	$html = isset( $_POST['html'] ) ? $_POST[ 'html' ] : '';
	$css = isset( $_POST[ 'css' ] ) ? $_POST[ 'css' ] : '';
	$imgLarge = isset( $_POST[ 'large_image' ] ) ? $_POST[ 'large_image' ] : '';
	$imgMedium = isset( $_POST[ 'medium_image' ] ) ? $_POST[ 'medium_image' ] : '';
	$imgSmall = isset( $_POST[ 'small_image' ] ) ? $_POST[ 'small_image' ] : '';
	$imgMicro = isset( $_POST[ 'micro_image' ] ) ? $_POST[ 'micro_image' ] : '';

	//Remove the base url of the site
	$url = get_site_url();
	$imgLarge = str_replace( $url, '', $imgLarge );
	$imgMedium = str_replace( $url, '', $imgMedium );
	$imgSmall = str_replace( $url, '', $imgSmall );
	$imgMicro = str_replace( $url, '', $imgMicro );

	// Save meta options to db
	update_post_meta( $post_id, 'html', $html );
	update_post_meta( $post_id, 'css', $css );
	update_post_meta( $post_id, 'large_image', $imgLarge );
	update_post_meta( $post_id, 'medium_image', $imgMedium );
	update_post_meta( $post_id, 'small_image', $imgSmall );
	update_post_meta( $post_id, 'micro_image', $imgMicro );

	}


add_action( 'admin_enqueue_scripts', 'load_ix_hero_admin_script' );
function load_ix_hero_admin_script () {
	global $typenow, $ixheroversion;

	if ( $typenow == 'ix_hero' ) {
		wp_enqueue_media();

		wp_register_script( 'ix_hero_admin_script', plugin_dir_url( __FILE__ ) . 'js/ix_hero_admin_script.min.js', array( 'jquery' ), $ixheroversion);
		wp_enqueue_script( 'ix_hero_admin_script' );
	}
}

// Front End Part


// Add Shortcode
add_shortcode( 'ix-hero', 'ix_hero_generate' );
function ix_hero_generate( $atts ) {

	// Attributes
	$a = shortcode_atts( array( 'id' => '' ), $atts );

	if ( $a['id'] == '' or intval($a['id']) == 0 ) {
		return '<h3>'. __( "Your iX Hero Image Shortcode is invalid.", 'ix-hero' ) .'</h3>';
	}
	$html = get_post_meta( intval($a['id']), 'html', true );
	if( empty( $html ) ) {
		return '<h3>'. __( "The iX Hero Image does not exists, please update the Shorcode.", 'ix-hero' ) .'</h3>';
	}

	$html = '<div class="hero-container"> <div class="hero"> <div class="dummy"> </div>' . $html . '</div> </div>';
	return $html;
}


// Register Style
add_action('wp_head', 'ix_hero_add_faux_css', 100);

function ix_hero_add_faux_css() {

	//Regex post_content to see if there are any ix hero shortcodes
	global $post;
	$matches;
	preg_match_all( "/\[ix\-hero id=['|&quot;|\"]+([0-9]+)['|&quot;|\"]+\]/", $post->post_content, $matches );
	//if we have at least one match inject the css into the header
	if ( count( $matches[1] ) > 0) {
		$inlineCss = '';
		$url = get_site_url();
		foreach ( $matches[1] as $id ) {
			$inlineCss = $inlineCss . get_post_meta( intval( $id ), 'css', true );
			//Get all the image variation from the db
			$large_image = get_post_meta( $id, 'large_image', true );
			$medium_image = get_post_meta( $id, 'medium_image', true );
			$small_image = get_post_meta( $id, 'small_image', true );
			$micro_image = get_post_meta( $id, 'micro_image', true );

			//Replace the placeholder in the css file with absolutized image url or with external url
			if ( $large_image[0] == "/") {
				$inlineCss = str_replace( '###LargeImage###', $url . $large_image, $inlineCss );
			} else {
				$inlineCss = str_replace( '###LargeImage###', $large_image, $inlineCss );
			}

			if ( $medium_image[0] == "/") {
				$inlineCss = str_replace( '###MediumImage###', $url . $medium_image, $inlineCss );
			} else {
				$inlineCss = str_replace( '###MediumImage###', $medium_image, $inlineCss );
			}

			if ( $small_image[0] == "/") {
				$inlineCss = str_replace( '###SmallImage###', $url . $small_image, $inlineCss );
			} else {
				$inlineCss = str_replace( '###SmallImage###', $small_image, $inlineCss );
			}

			if ( $micro_image[0] == "/") {
				$inlineCss = str_replace( '###MicroImage###', $url . $micro_image, $inlineCss );
			} else {
				$inlineCss = str_replace( '###MicroImage###', $micro_image, $inlineCss );
			}
		}

		echo ( "<style>" );
		echo ( $inlineCss );
		echo ("</style>");

	}




}



?>