jQuery(document).ready(function($){
 
    // Prepare selection window
    var select_window = {};
    select_window.type = "";

    function openImgSelector(type) {
 
        // If selection window exists and is of the same type just open it.
        if ( select_window.type === type ) {
            select_window.open();
            return;
        }
 
        // Populate select_window object
        select_window = wp.media.frames.select_window = wp.media({
            title: "Select " + type + " image" ,
            button: { text:  "Select" },
            library: { type: "image" }
        });
        
        // Save the image type
        select_window.type = type;
        
        // Runs when an image is selected.
        select_window.on("select", function(){
 
            // Transform the selection to JSON
            var img = select_window.state().get("selection").first().toJSON();
 
            // Asign url of the selection to the propriet input 
            $("#" + type + "_image").val(img.url);
        });
 
        // Opens the media library frame.
        select_window.open();
}

 
    $("#large-image-button").click(function(e){
        // Prevent default action
        e.preventDefault();
        openImgSelector("large");

    });

    $("#medium-image-button").click(function(e){
        // Prevent default action
        e.preventDefault();
        openImgSelector("medium");

    });

    $("#small-image-button").click(function(e){
        // Prevent default action
        e.preventDefault();
        openImgSelector("small");

    });

    $("#micro-image-button").click(function(e){
        // Prevent default action
        e.preventDefault();
        openImgSelector("micro");

    });            
});