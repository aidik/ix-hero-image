# iX Hero Image
Plugin for easy insertion of hero images with the use of shortcodes.

## Requirements
To run iX Hero Image you need to have:
* Wordpress (so far tested only on 4.4)

## Install 
Upload the zip file from the **release** directory, or a different source to your Wordpress Plugins using the standard form at _/wp-admin/plugin-install.php?tab=upload_

![Wordpress Manual Plugin Install][01]

Don't forget to activate the newly installed plugin.

## Usage
After the installation and activation you will notice a new item in the left sidebar menu. Click on it and you will be taken to the iX Hero Images overview.
![iX Hero Images overview][02]

Use the "Add new" button and edit the new iX Hero Image to your liking.
![New iX Hero Image page][03]

Notice that in the CSS code specific placeholders are used for the urls of the images. Use the standard image uploader and selector to select images. Copy the shortcode to insert into your posts.
![Standard image uploader and selector][04]

To display the newly created iX Hero Image insert shortcode into the post.
![Insert shortcode into the post][05]


## Report Bugs
Please, [report all the bugs!](https://bitbucket.org/aidik/ix-hero-image/issues)

## Contribution
Locally clone this repository and run:
	`npm install`

To create a release run:
	`grunt release`

Another useful **Grunt** task is `watch`.

## License

[MIT License](https://ixsystems.com) © Václav Navrátil

[01]: https://bytebucket.org/aidik/ix-hero-image/raw/5ddef8737520ba82ee76f832ec3eda45eb14e0de/doc/img/01.png "Wordpress Manual Plugin Install"

[02]: https://bytebucket.org/aidik/ix-hero-image/raw/5ddef8737520ba82ee76f832ec3eda45eb14e0de/doc/img/02.png "iX Hero Images overview"

[03]: https://bytebucket.org/aidik/ix-hero-image/raw/5ddef8737520ba82ee76f832ec3eda45eb14e0de/doc/img/03.png "New iX Hero Image page"

[04]: https://bytebucket.org/aidik/ix-hero-image/raw/0d11d306fcf082276084fd74296e20cc4a01a66d/doc/img/04.png "Standard image uploader and selector"

[05]: https://bytebucket.org/aidik/ix-hero-image/raw/5ddef8737520ba82ee76f832ec3eda45eb14e0de/doc/img/05.png "Insert shortcode into the post"